import $ from 'jquery';
import Backbone from 'backbone';
import Pelican from 'pelican';

const Router = Pelican.Router.extend({

    // automatic screen routes
    appRoutes: {
        '': '',
        'discharged': 'DischargedScreen',
        'discharged/tv': 'TvScreen',
        'intro': 'IntroScreen',
        'videoplayer/:id': 'VideoPlayerScreen',
        'tv': 'TVScreen',
        'home': 'PrimaryScreen',
        'movie/:id': 'MovieSynopsisScreen',
        'movieplayer/:id': 'MoviePlayerScreen',
        'home/entertainment/free-movies': 'MovieScreen',
        'home/entertainment/free-movies-spanish': 'MovieScreen',
        'home/entertainment/watch-tv': 'TVGuideScreen',
        'home/entertainment/relaxation-playlist': 'PlayListPlayerScreen',
        'home/dine/order-meals': 'OrderMealsScreen',
        'home/my-requests/room-temperature': 'RoomTempScreen',
        'home/my-care/my-medication': 'MyMedicationScreen',
        'home/my-care/my-education': 'MyProgramScreen',
        'home/my-care/health-education-library': 'EdLibraryScreen',
        'education/:razuna': 'EDSynopsisScreen',
        'educationvideo/:razuna': 'EpicVideoPlayerScreen',
        'relaxation/:razuna': 'RelaxationPlayerScreen',
        'webpage': 'WebPageScreen',
        'ehospital': 'EHospitalScreen',
        'home/:group/:id': 'SecondaryScreen'
    },

    // To avoid writing too many switch/case, we let webpack pack everything in ./screens/ and require them dynamically.
    // This function will be called during routing check.  Implement this function so it reflects to your screen folder
    // structure.
    getScreen: function (name) {
        try {
            var ScreenClass = require('./screens/' + name.toLowerCase() + '.js').default;
        }
        catch (e) {
            console.log('Screen not found. ' + e.toString());
            return undefined;
        }
        return ScreenClass;
    },

    // this function will be called right after router started
    onStart: function () {
        var startPath = this.getFullPath();
        if (App.data && App.data.patient && App.data.patient.mrn) { // admitted
            if (!startPath || startPath.length < 2) {
                if (App.data.patient.patientPreferences) { // check if accepted term
                    var terms = $.grep(App.data.patient.patientPreferences, function (e) {
                        return e.key == 'term';
                    });
                    if (terms.length > 0) {
                        window.location.href = '#home';
                    }
                    else {
                        window.location.href = '#intro';
                    }
                }
                else {
                    window.location.href = '#intro';
                }
            }
            else if (startPath != 'home' && startPath != 'intro') {
                // deep linking for developer only.  always display home page first then this page
                App.layout.closeAllScreens();
                window.location.href = '#home';
                setTimeout(function () {
                    window.location.href = '#' + startPath;
                }, 50);
            }
        }
        else { // discharged
            window.location.href = '#discharged';
        }
        console.log('landing route:' + this.getFullPath());
    }

});

export default Router;