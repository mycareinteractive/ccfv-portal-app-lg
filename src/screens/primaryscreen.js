import Pelican from 'pelican';
import ApiHelper from '../helpers/apihelper';

var packageJson = require("../../package.json");

require('../css/common.css');
require('../css/dialog.css');
require('../css/primary.css');


var template = require('../templates/primaryscreen.hbs');

const PrimaryScreen = Pelican.Screen.extend({

    className: 'primary',

    template: template,

    keyEvents: {
        'POWER': true,
        'EXIT': true,
        'BACK': true,
        'HOME': true,
        'CLOSE': true
    },

    events: {
        'click #my-language': 'selectMyLanguage',
        // language dialog
        'focus #lang-english': 'highlightEnglish',
        'focus #lang-spanish': 'highlightSpanish',
        'click #lang-english': 'selectLanguage',
        'click #lang-spanish': 'selectLanguage'
    },

    widgets: {
        dialog: {
            widgetClass: Pelican.Dialog,
            selector: '#dialog',
            options: {hidden: true}
        },
        mainmenu: {
            widgetClass: Pelican.DropdownMenu,
            selector: '#menu'
        },
        datetime: {
            widgetClass: Pelican.DigitalClock,
            selector: '#date',
            options: {format: 'h:MM TT, dddd, mmm. d, yyyy', locale: 'en'}
        }
    },

    onInit: function (options) {
        var slug = App.upserver.baseSlug.substr(0, App.upserver.baseSlug.length - 1);
        var self = this;

        // dynamic set clock language
        self.getWidget('datetime').locale = App.data.language;

        // get menu from server
        var contents = new Pelican.Collections.ContentCollection();
        console.log('getting content from: ' + slug);
        contents.fetch({
            data: {
                slug: slug,
                depth: 2
            }
        })
            .done(function () {
                var content = contents.toJSON();
                var model = {menu: content};
                self.getWidget('mainmenu').model.set(model);
                self.click('#menu1');
            })
            .fail(function () {
                self.$('.major').html('<p>Unable to get menu content from server.  Please contact your nurse.</p>').css('text-align', 'center');
            });
    },

    updatePatient: function () {
        if (App.data && App.data.patient && App.data.patient.patientProfile) {
            this.$('#patient-name').text(App.data.patient.patientProfile.fullName);
        }
    },

    updateRoom: function () {
        if (App.data && App.data.device && App.data.device.deviceProvision && App.data.device.deviceProvision.location) {
            var location = App.data.device.deviceProvision.location;
            this.$('#room-phone').text(location.phone);
            var room = location.room;
            if (location.bed != '00') {
                room = room + ' ' + location.bed;
            }
            this.$('#room-number').text(room);
        }
    },

    onAttach: function () {
        var self = this;

        this.updatePatient();
        this.updateRoom();
        upserver.on(upserver.events.patient.profileChanged, self.updatePatient.bind(this));
        upserver.on(upserver.events.device.locationUpdated, self.updateRoom.bind(this));

        var pathnames = location.pathname.split('/');
        var appName = pathnames[pathnames.length - 2] || '';
        this.$('#app-version').text(appName + ' ' + packageJson.version);

        //language dialog
        var p1 = 'Select a Language:';
        var p2 = 'Translations are provided wherever possible. Not all content has been translated. If you need additional help, please talk to your clinical staff.';
        var p3 = 'For help, ask a member of your care team or press the Nurse button.';

        var langHtml =
            '<div class="english">' +
            '<p class="text4">' + p1 + '</p><br>' +
            '<p class="text2"><br>' + p2 + '<br><br></p>' +
            '<p class="text1">' + p3 + '</p>' +
            '</div>' +
            '<div class="spanish">' +
            '<p class="text4">' + i18next.t(p1, {lng: 'es'}) + '</p><br>' +
            '<p class="text2"><br>' + i18next.t(p2, {lng: 'es'}) + '<br><br></p>' +
            '<p class="text1">' + i18next.t(p3, {lng: 'es'}) + '</p>' +
            '</div>';

        var model = {
            buttons: [
                {id: 'lang-english', text: 'English'},
                {id: 'lang-spanish', text: 'Spanish'}
            ],
            content: langHtml
        };
        this.getWidget('dialog').model.set(model);

    },

    onScreenShow: function() {
        $('body').removeClass('video');
    },

    selectMyLanguage: function (e) {
        // cancel the link
        e.preventDefault();
        e.stopImmediatePropagation();

        // show dialog
        this.getWidget('dialog').show();

        var currLang = App.data.language;
        if (currLang == 'es') {
            this.focus('#lang-spanish');
        }
        else {
            this.focus('#lang-english');
        }
    },

    highlightEnglish: function () {
        this.getWidget('dialog').$('#instruction div').hide();
        this.getWidget('dialog').$('#instruction div.english').show();
    },

    highlightSpanish: function () {
        this.getWidget('dialog').$('#instruction div').hide();
        this.getWidget('dialog').$('#instruction div.spanish').show();
    },

    selectLanguage: function (e) {
        var self = this;
        var id = $(e.target).attr('id');
        var lang = 'en';
        if (id == 'lang-spanish') {
            lang = 'es';
        }

        this.getWidget('dialog').hide();

        if (App.data.language != lang) {
            // change langauge. reload home page
            App.data.language = lang;   // global option
            window.upserver.setLanguage(lang);  // set client API cookie
            window.upserver.locale = lang;  // set client API language header
            ApiHelper.setPreference('language', lang); // save to server

            // load new resource
            i18next.changeLanguage(lang, function () {
                App.layout.closeAllScreens();
                // this is a trick to force it go to '#home' even URL is already '#home'
                Backbone.history.loadUrl(Backbone.history.fragment);
            });

            // analytics
            App.tracker.event('language', 'switch', lang);
        }
    }
});

export default PrimaryScreen;