import Pelican from 'pelican';
require('../css/common.css');
require('../css/movies-synopsis.css');
var template = require('../templates/moviesynopsisscreen.hbs');

const MovieSynopsisScreen = Pelican.Screen.extend({

    className: 'moviesynopsis',

    video: new Pelican.Models.Content(),

    template: template,

    keyEvents: {},

    events: {
        'click .back-button': 'back'
    },

    widgets: {

        buttons: {
            widgetClass: Pelican.ButtonGroup,
            selector: '#buttons',
            options: {orientation: 'vertical', preRender: true}
        }

    },

    calcDuration: function (duration) {
        if(!duration)
            return '00:00';
        var str = '';
        if(duration>=3600) {
            var h = Math.floor(duration/3600);
            str += h + ':';
            duration = duration%3600;
        }
        if(duration>=60) {
            var mm = Math.floor(duration/60);
            str += (mm<10?'0':'') + mm + ':';
            duration = duration%60;
        }
        var ss = duration;
        str += (ss<10?'0':'') + ss;
        return str;
    },

    getVideo: function(videoId){
        var self = this;
        var defer = jQuery.Deferred();

        var vid = videoId || self.vid;

        self.video.fetch({
            uri: '/contents',
            data:{
                id: vid
            }
        })
            .done(function(){
                self.state = 'loaded';
                console.log(self.video);
                defer.resolve();
            })
            .fail(function(f){
                defer.reject(f.errorMessage);
            });

        return defer.promise();
    },


    onInit: function () {
        var self = this;
        console.log('video queries: ' + JSON.stringify(self.queries));
        $('body').addClass('video');
        if (self.params.length > 1) {
            self.vid = self.params[0];
        }

        if (self.queries && self.queries['vid']) {
            self.vid = self.queries['vid'];
        }

        var screen = self.path.split('/')[2];
        console.log('SCREEN: ' + screen);
        var i18t = '';
        if(screen == 'free-movies'){
            i18t = 'Free Movies';
            self.$('.page-title').html(i18n.t(i18t));
        } else if(screen == 'free-movies-spanish'){
            if(App.data.language == 'en') {
                i18t = 'Free Movies - Spanish';
            } else if(App.data.language == 'es') {
                i18t = 'Free Movie - Spanish';
            }
            self.$('.page-title').html(i18n.t(i18t));
        }

        console.log('vid = ' + self.vid);

    },


    onScreenShow: function () {

        var self = this;
        self.lastPosition = 0;
        self.getVideo()
            .done(function () {

                self.blur('a');
                self.focus('#restart');

                if (self.video && self.video.attributes) {
                    console.log('!-- We are done getting Movie Data --!');
                    self.bookmarkIds = self.video.get('bookmarkIds') || [];
                    self.bookmarkId = self.video.get('bookmarkIds')[0];

                    self.runTime = self.calcDuration(self.video.get('metadatas').runtime);
                    self.title = self.video.get('metadatas').title;
                    self.rating = self.video.get('metadatas').rating || "NR";
                    self.description = self.video.get('metadatas').description;
                    self.thumbnail = self.video.get('thumbnail');
                }


                self.$('#heading1 img').attr('src', self.thumbnail);
                self.$('.movie-title').html(self.title);
                self.$('.movie-rating').html(self.rating);
                self.$('.movie-run-time').html(self.runTime);
                self.$('.movie-synopsis').html(self.description);
                self.$('.movie-instruction').show();
                self.$('#restart').attr('href', '#movieplayer/' + self.params[0]);

                if(self.bookmarkIds.length > 0) {
                    $('#resume').show();
                    var bookmark = new Pelican.Models.Bookmark({id: self.bookmarkId});
                    self.lastPosition = 0;
                    bookmark.fetch()
                        .done(function () {
                            console.log(bookmark);
                            self.lastPosition = bookmark.attributes.lastPosition;

                            self.duration = self.calcDuration(self.runTime);
                            self.$('#heading1 .text-large').html(self.title);
                            self.$('#resume').attr('href', '#movieplayer/' + self.params[0] + '?start=' + self.lastPosition* 1000 );
                        });
                }
            });
    },

    onAttach: function () {
    }
});

export default MovieSynopsisScreen;