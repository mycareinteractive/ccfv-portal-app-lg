import Pelican from 'pelican';
import VideoPlayerScreen from './videoplayerscreen';

require('../css/dialog.css');

const PlayListPlayerScreen = VideoPlayerScreen.extend({

    videoParam: {
        repeatCount: 999  // infinite loop
    },

    autoStart: true,
    bookmarking: false,
    allowFastforward: false,
    allowRewind: false,
    allowPause: false,
    analytics: true,

    creditLength: 1000,

    type: 'relaxation',

    bookmarkIds: [],
    renditions: [],

    media: [],
    playList: [],
    repeatCount: 0,

    onInit: function (options) {
        // VideoPlayerScreen.prototype.onInit.call(this, options);
        var self = this;

        // make sure we destroy previous media and play lists arrays
        // upon returning, videos could have been added or removed.
        self.media = [];
        self.playList = [];

        $('body').addClass('video');

        console.log('THESE ARE THE OPTIONS');
        console.log(options);

        // the 3 lines below should always be run
        self._setParams();
        self._initializeMediaEvents();

        var folders = new Pelican.Collections.ContentCollection();
        var slug = self.path.replace('home/', App.upserver.baseSlug);
        console.log('Slug: ' + self.path);

        folders.fetch({
            data: {
                slug: slug,
                depth: 1
            }
        })
            .done(function () {
                console.log('Movie ROUTE SEGMENT: ' + slug);
                var content = folders.toJSON();

                /**
                 * This is down and dirty. Should be done more neat.
                 * Depending on portal, path index could be different.
                 */
                var screen = self.path.split('/')[2];
                console.log('SCREEN: ' + screen);
                console.log(content);

                self.buildPlayList(content)
                    .done(function(ctime){
                        console.log("CTIME: " + ctime);
                        self.duration = ctime;
                        self.startRelaxationChannel();
                    });
            });
    },

    rebuildPlayer: function(){
        console.log('we are rebuilding the video player');
        var self = this;
        var hasRebuilt = false;
        var defer = jQuery.Deferred();

        App.device.stopMedia()
            .always(function () {
                self.onStop(false);
                // After video stops, oddly the TV will start playing
                App.device.stopChannel();
                hasRebuilt = true;
                defer.resolve();
                console.log("!! REBUILDING :: STOP MEDIA CALL BACK !!");
            })
            .fail(function (m) {
                console.log('stopMedia failed in VideoPLayerScree.stop() with error message: ' + m);
                defer.reject(m);
            });

        // while(!hasRebuilt) { /* hang here until wee are rebuilt*/ }
        // console.log("!! FINISHED REBUILDING !!");
        return defer.promise();
    },

    onPlayStart: function () {
        // when start calculate the appropriate start position
        // this.calculateStartPosition();
        this.$el.css('background', 'transparent');
        console.log("We have started new media.");
    },

    onPlayEnd: function () {
        // when relaxation video reaches end, replay
        // this.seek(0);
        var self = this;
        self.rebuildPlayer()
            .done(function(){
                self.startRelaxationChannel();
            });
        return true;    // return true so videoplayer screen will NOT stop the stream
    },

    onStop: function () {
        return false;
    },

    /*onSeekDone: function(){
        console.log("********* We seeked to == " + this.startPosition + " *********");
    },*/

    calculateStartPosition: function () {
        this.duration = this.duration || 1800000;   // If duration not available use 30 minutes as default
        // Pretend 'channel' starts midnight, calculate what position should we be at this moment
        var now = new Date();
        var then = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0);
        var diff = now.getTime() - then.getTime();  // milliseconds since midnight
        this.startPosition = diff;
    },

    amendPlayList: function(ctime){
        var self = this;
        $.each(self.media, function(index, val){
            var vid = $.extend({}, val); ///
            var dur = vid.duration;

            vid.plstart = ctime + 1;
            vid.plend = ctime + dur;
            ctime += dur;
            self.playList.push(vid);
            /*console.log(
             "i: " + index,
             "vid: " + vid.contentId,
             "title: " + vid.title.substring(0, 24),
             "ctime: " + ctime,
             "start: " + vid.start,
             "end: " + vid.end
             );*/
        });
        return ctime;
    },

    getPointInTimeVideo: function(){
        console.log("Start Position: " + this.startPosition);
        console.log("PLAY LIST LENGTH: " + this.playList.length);
        for(var pi = 0; pi < this.playList.length; pi++) {
            // console.log(pi);
            var v = this.playList[pi];
            /*console.log(
             "pos: " + pos,
             "start: " + v.start,
             "end: " + v.end
             );*/

            if (this.startPosition > v.plstart && this.startPosition < v.plend) {
                console.log("WE GOTTA MATCH: " + v.title);
                return v;
            }
        }
    },

    startRelaxationChannel: function(){
        var self = this;

        console.log("Start Position: " + self.startPosition);
        // console.log('Media Play List: ', self.playList);

        self.calculateStartPosition();
        var relx = self.getPointInTimeVideo();
        $.extend(self, relx);
        console.log("DURATION: " + self.duration);
        self.startPosition = self.startPosition - self.plstart;

        console.log("Starting video at: " + self.startPosition);
        // self._setVurl();
        self.startVideo()
            .done(function(){
                self.seek(self.startPosition);
            });

        console.log("Relaxation Vid: ", relx.title);
    },

    /**
     *
     * @param content: list of videos from a folder that can bew used to build a playlist.
     */
    buildPlayList: function(content) {
        var self =this;
        var cc = 0;
        var defer = jQuery.Deferred();

        $.each(content, function(index, val){
            var vid = {};
            vid.contentId = val.contentId;
            self.getVideo(val.contentId)
                .done(function(){
                    vid.title = self.video.metadatas.title;
                    vid.duration = self.video.metadatas.runtime * 1000;
                    vid.vurl = self.getMediaUrl();
                    self.media.push(vid);
                    cc++;

                    if(cc >= content.length) {
                        // get 24 hours in seconds
                        var s24 = ((60*60)*24)*1000;
                        var ctime = -1;
                        console.log("We FINISHED adding items to the playlist. Seconds in 24 hours:" + s24);

                        // keep adding to playlist until 24 hours is reached
                        while(ctime < s24){
                            ctime = self.amendPlayList(ctime);
                            // console.log("We need more time.");
                        }

                        defer.resolve(ctime);
                    }
                })
            .fail(function(e){
                defer.reject(e.errorMessage);
            });

            console.log("We FINISHED looping through content and executing playlist build.");
        });

        return defer.promise();
    }

});

export default PlayListPlayerScreen;