import Pelican from 'pelican';

require('../css/video.css');
var template = require('../templates/videoplayerscreen.hbs');

const VideoPlayerScreen = Pelican.Screen.extend({

    className: 'video',
    template: template,

    noPageView: true,

    video: {}, // video data object
    vid: '', // video id
    vurl: '', // video url

    videoParam: {}, // additional video parameters

    state: 'stop',
    language: 'en',

    autoStart: true,
    bookmarking: true,
    allowFastforward: false,
    allowRewind: false,
    allowPause: true,
    analytics: true,
    startPosition: 0,

    creditLength: 10000,

    width: 1280,
    height: 720,
    posX: 0,
    posY: 0,

    type: 'video',
    ticks: 0,
    position: 0,
    duration: 0,
    renditionMap: {},
    repeatCount: 1,

    keyEvents: {
        'FFWD': 'fastForward',
        'RIGHT': 'fastForward',
        'LEFT': 'rewind',
        'RWND': 'rewind',
        'PAUSE': 'playPause',
        'PLAY': 'playPause',
        'INFO': 'stop',
        'STOP': 'stop'

    },

    events: {},
    widgets: {},

    getVideo: function (videoId) {
        var self = this;
        var defer = jQuery.Deferred();

        var vid = videoId || self.vid;

        var tempVid = new Pelican.Models.Content();

        tempVid.fetch({
            uri: '/contents',
            data: {
                id: vid
            }
        })
            .done(function () {
                self.state = 'loaded';
                self.video = tempVid.toJSON();

                var hasBookmarks = (self.video.bookmarkIds.length > 0) ? true : false;

                if (self.video && self.video.metadatas && self.video.metadatas.runtime) {
                    self.duration = self.video.metadatas.runtime * 1000;
                    console.log('duration from metadata: ' + self.duration);
                }

                var title = vid;
                if (self.video && self.video.metadatas && self.video.metadatas.title) {
                    title = self.video.metadatas.title;
                }
                App.tracker.pageView(self.path, title);

                if(self.bookmarking && hasBookmarks) {
                    console.log('VideoPlayerScreen: These are the bookmarks from VideoPlayerScreen');
                    console.log(self.video.bookmarkIds);
                } else if(self.bookmarking && !hasBookmarks) {
                    console.log('VideoPlayerScreen: Dang it, Jim! There are no bookmarks!');
                    var nbm = new Pelican.Models.Bookmark();
                    nbm.save({
                        contentId: vid,
                        type: "movie"
                    })
                        .done(function () {
                            console.log('Were we able to save the bookmark? ');
                            console.log('WE are done saving the bookmark');
                        });
                }

                defer.resolve();
            })
            .fail(function (f) {
                defer.reject(f.errorMessage);
            });

        return defer.promise();
    },

    getMediaUrl: function () {
        var self = this;

        var vobj = self.video.renditions[0];

        if (App.config.originalUrl) {
            self.vurl = vobj.metadatas.originalUrl || vobj.metadatas.url;
        } else {
            self.vurl = vobj.metadatas.url || vobj.metadatas.originalUrl;
        }

        console.log('MEDIA OBJECT');
        console.log(vobj);

        return self.vurl;
    },

    stop: function (destroyScreen) {
        var self = this;

        $.doTimeout('queue');
        $.doTimeout('video progress');
        $.doTimeout('video keep alive');

        // don't get loopy.
        if (self.state == 'stop')
            return false;

        /**
         * Ideally this would be called inside a defer of stopMedia.
         * However, this was causing issues. The main one being an
         * inifinte loop. As long as we set statee before calling onStop()
         * we avoid the infinte loopo, as well.
         */
        self.state = 'stop';

        console.log('We pressed the stop button');

        try {
            App.device.stopMedia()
                .always(function () {
                    self.onStop(destroyScreen);
                    // After video stops, oddly the TV will start playing
                    App.device.stopChannel();
                })
                .fail(function (m) {
                    console.log('stopMedia failed in VideoPLayerScree.stop() with error message: ' + m);
                });

        }
        catch (e) {
            console.log('error when stopping player: ' + JSON.stringify(e));
        }

        var creditLength = this.creditLength;
        if (Math.floor(this.position) + creditLength > this.duration) {
            this._trackEvent('complete');
        }
    },

    seek: function (timeInMs) {
        var defer = jQuery.Deferred();
        App.device.setMediaPosition(timeInMs)
            .done(function () {
                defer.resolve();
                console.log("The movie has changed its position to: " + timeInMs);
            })
            .fail(function(m){
                console.log("This is all messed up!");
                defer.reject(m);
            });
        return defer.promise();
    },

    tick: function () {
        var self = this;

        this.ticks++;
        if (this.state == 'play' || this.state == 'loaded') {
            this.position += 1000;
        }

        // calibrate the position every 10 seconds
        if (self.ticks % 10 == 1) {
            App.device.getMediaPosition()
                .done(function (positionInMs) {
                    self.position = positionInMs;
                    console.log('Position: ' + self.position);
                });
        }

        // always return true to fire next tick
        return true;
    },

    startVideo: function () {
        var self = this;
        var defer = jQuery.Deferred();

        var vurl = self.vurl;
        console.log(vurl);

        // Delay the video frames for a bit
        self.$el.css('background', '#000000');

        $.extend(self.videoParam, {
            url: vurl,
            repeatCount: self.repeatCount
        });

        App.device.playMedia(self.videoParam)
            .done(function () {
                defer.resolve();
                console.log('WE HAVE SUCCESSFULLY STARTED THE VIDEO');
            })
            .fail(function (f) {
                defer.reject(f.errorMessage);
                console.log('MOVIE PLAY HAS FAILED: ' + f.errorMessage)
            });

        // start a clock for tracking
        $.doTimeout('video progress');
        $.doTimeout('video progress', 1000, self.tick.bind(self));

        // a timer to keep analytics session from timing out
        $.doTimeout('video keep alive', 600000, function () {
            App.tracker.heartbeat();
            return true;
        });

        this._trackEvent('play');

        return defer.promise();
    },

    playPause: function () {
        if (!this.allowPause)
            return false;

        var self = this;

        $.doTimeout('queue');

        App.device.getMediaPosition()
            .done(function (positionInMs) {
                self.position = positionInMs;
            });

        if (self.state == 'play' || self.state == 'loaded') {
            App.device.pauseMedia()
                .done(function () {
                    self.state = 'pause';
                });
        }
        else {
            App.device.resumeMedia()
                .done(function () {
                    self.state = 'play';
                });
        }
    },

    fastForward: function () {
        if (!this.allowFastforward)
            return false;

        var self = this;
        var defer = jQuery.Deferred();
        console.log('You are pressing FFWD.');

        $.doTimeout('queue');

        if (self.state == 'fastForward') {
            App.device.resumeMedia()
                .done(function () {
                    self.state = 'play';
                });
        }
        else {
            self.state = 'fastForward';

            $.doTimeout('queue', 700, function () {
                self.position += 20000;
                App.device.setMediaPosition(self.position)
                    .done(function () {
                        defer.resolve();
                    })
                    .fail(function (f) {
                        defer.reject(f);
                        console.log('We were not able to set the Media Position');
                    });
                return true;
            });
        }

        return defer.promise();
    },

    rewind: function () {
        if (!this.allowRewind)
            return false;

        var self = this;
        var defer = jQuery.Deferred();
        console.log('You are pressing REWIND.');

        $.doTimeout('queue');

        if (self.state == 'rewind') {
            App.device.resumeMedia()
                .done(function () {
                    self.state = 'play';
                });
        }
        else {
            self.state = 'rewind';
            $.doTimeout('queue', 700, function () {
                self.position -= 20000;
                App.device.setMediaPosition(self.position)
                    .done(function () {
                        defer.resolve();
                    })
                    .fail(function (f) {
                        defer.reject(f);
                        console.log('We were not able to set the Media Position');
                    });
                return true;
            });
        }

        return defer.promise();
    },

    onInit: function (options) {
        var self = this;

        $('body').addClass('video');

        console.log('THESE ARE THE OPTIONS');
        console.log(options);

        // the 3 lines below should always be run
        self._setParams();
        self._initializeMediaEvents();

        self.getVideo()
            .done(function () {
                self._setVurl();
                self.startVideo();
            });

        console.log('PELICAN PLATFORM: ' + App.device.getPlatform());
        console.log('Initialize FETCH for video data');
    },

    onScreenShow: function () {
        console.log('onScreenShow: Showing the Video PLayer screen');
        $('body').addClass('video');
    },

    onScreenHide: function () {
        console.log('onScreenHide: Hiding the Video Player screen');
        $('body').removeClass('video');
        this.stop(true);

        this._killMediaEvents();
    },

    updateBookmark: function (action, pos) {
        var self = this;

        var position = Math.floor(self.position / 1000);
        if (pos) {
            position = Math.floor(pos / 1000);
        }
        var action = {
            action: action,
            position: position,
            duration: Math.floor(self.duration / 1000),
            param: ''
        };

        // for every bookmark with this video, update them all
        $.each(self.bookmarkIds, function (i, bid) {
            upserver.api('/me/bookmarks/' + bid + '/action', 'POST', action);
            console.log('We have updated bookmark (' + bid + ') with action: ' + action.action);
        });
    },

    /*
     * The methods below are ment to be overriden and are used to hook into
     * the media players native callback functions.
     * */

    onBeforeStop: function () {
    },
    onStop: function () {
        this.back();
    },
    onPlayStart: function () {
    },
    onPlayEnd: function () {
    },
    onErrorInPlaying: function () {
    },
    onBufferFull: function () {
    },
    onFileNotFound: function () {
    },
    onNetworkDisconnected: function () {
    },
    onNetworkBusy: function () {
    },
    onNetworkCannotProcess: function () {
    },
    onSeekDone: function () {
    },

    /**
     * PRIVATE METHODS
     */

    _eventHandler: function (param) {
        var self = this;
        switch (param.eventType) {
            case 'play_start':
                console.log('EVENT: play_start -- The video has started to play.');

                self.onPlayStart();

                App.device.getMediaDuration()
                    .done(function (duration) {
                        self.duration = duration;
                        console.log('duration from media: ' + self.duration);
                    });

                App.device.getMediaPosition()
                    .done(function (positionInMs) {
                        self.position = positionInMs;
                    });

                // this timer delay a bit then background transparent so that we don't see the first frame jumping when seek
                $.doTimeout('video background');

                if (self.state == 'loaded') {
                    self.state = 'play';
                    self.position = self.startPosition;
                    console.log("The VIDEO has LOADED!");

                    if (self.startPosition > 0) {
                        console.log('play_start: start position has been found. we are seeking to ' + self.startPosition);
                        self.seek(self.startPosition);

                        $.doTimeout('video background', 2000, function () {
                            self.$el.css('background', 'transparent');
                        });
                    }
                    else {
                        $.doTimeout('video background', 500, function () {
                            self.$el.css('background', 'transparent');
                        });
                    }
                }
                break;
            case 'play_end':
                console.log('EVENT: play_end -- The video has ended.');
                // force position to end because LG player will reset position to 0.0
                self.position = self.duration;

                if (self.state == 'stop') {
                    return;
                }

                var keepPlaying = self.onPlayEnd();
                if (!keepPlaying)
                    self.stop();
                break;
            case 'error_in_playing':
                self.stop();
                console.log('EVENT: error_in_playing -- There has been an error in play back.');
                self.onErrorInPlaying();
                break;
            case 'buffer_full':
                self.stop();
                console.log('EVENT: buffer_full -- The buffer is full.');
                self.onBufferFull();
                break;
            case 'file_not_found':
                console.log('EVENT: file_not_found -- The file could not be found.');
                self.onFileNotFound();
                break;
            case 'network_disconnected':
                console.log('EVENT: network_disconnected -- The network as disconnected.');
                self.onNetworkDisconnected()
                break;
            case 'network_busy':
                console.log('EVENT: network_busy -- The network is currently busy.');
                self.onNetworkBusy();
                break;
            case 'network_cannot_process':
                console.log('EVENT: network_cannot_process -- The network cannot process.');
                self.onNetworkCannotProcess();
                break;

            /*
             * TODO: Cant do seek_done yet because of how we are handling ff/rwd trick modes
             * We are essentially executing multiple seeks to mimic ff/rwd
             * This will also interfere with setPlayPosition
             * To maintain simplicity, we should avoid using this at all cost.
             */
            case 'seek_done':
                console.log('EVENT: seek_done -- The seek has finished');
                App.device.getMediaPosition()
                    .done(function (positionInMs) {
                        self.position = positionInMs;

                        if (self.position < 10000 && self.state == 'rewind') {
                            console.log('We are stopping the rewind because we have reached the beginning. We are going to continue and play.');
                            $.doTimeout('queue');
                            // we do this so that we can start at the beginning
                            self.seek(0)
                                .done(function () {
                                    App.device.resumeMedia()
                                        .done(function () {
                                            console.log('We have successfully resumed the media after reaching the beginning.');
                                            self.state = 'play';
                                        });
                                });
                        }
                    });
                break;
            /*
             if(this.state == 'fastForward') {
             App.device.stopMedia()
             .always(function () {
             // After video stops, oddly the TV will start playing
             App.device.stopChannel();
             self.state = 'stop';
             });
             } else {
             App.device.resumeMedia()
             .done(function(){
             self.state = 'play';
             });
             }

             self.onSeekDone();
             break;
             */
        }

    },

    _initializeMediaEvents: function () {
        var self = this;

        // onMediaEvent is a binded function of _eventHandler() that always has "this" pointing to player
        this.onMediaEvent = this._eventHandler.bind(this);
        document.addEventListener('media_event_received', this.onMediaEvent, false);
    },

    _setVurl: function () {
        this.getMediaUrl();
    },

    _setParams: function () {
        var self = this;

        // all supported query parameters
        if (self.queries) {
            if (self.queries['vid']) {
                self.vid = self.queries['vid'];
            }
            if (self.queries['start']) {
                self.startPosition = parseInt(self.queries['start']);
            }
            if (self.queries['fastforward']) {
                self.allowFastforward = self.queries['fastforward'];
            }
            if (self.queries['rewind']) {
                self.allowRewind = self.queries['rewind'];
            }
            if (self.queries['analytics']) {
                self.analytics = self.queries['analytics'];
            }
            if (self.queries['autoStart']) {
                self.autoStart = self.queries['autoStart'];
            }
            if (self.queries['creditLength']) {
                self.creditLength = self.queries['creditLength'];
            }
        }

        if (self.params.length > 1) {
            self.vid = self.params[0];
        }
    },

    _killMediaEvents: function () {
        document.removeEventListener('media_event_received', this.onMediaEvent, false);
    },

    _trackEvent: function (action) {
        if (!this.analytics) {
            return;
        }
        var label = this.vid;
        if (this.video.metadatas) {
            label += '|' + this.video.metadatas.title;
        }
        var options = {};
        options['nonInteraction'] = true;
        App.tracker.event(this.type, action, label, 0, options);
    },

    _startBookmarking: function (time, action) {
        var self = this;
        $.doTimeout('bookmark ping', time, function () {
            self.updateBookmark(action);
            console.log('Bookmark PING');

            return true;
        });

        console.log('We have started bookmarking every ' + time + ' milliseconds.');
    },
    _stopBookmarking: function () {
        $.doTimeout('bookmark ping');
        console.log('We have stopped bookmarking.');
    },

    _goHome: function () {
        App.router.go('home');
    }
});

export default VideoPlayerScreen;