import Pelican from 'pelican';

require('../css/common-movies.css');
require('../css/movies.css');

var template = require('../templates/moviescreen.hbs');

const MovieScreen = Pelican.Screen.extend({

    className: 'movies',

    template: template,

    keyEvents: {},

    events: {
        'click .back-button': 'back',
        'click .folder': 'selectFolder',
        'focus #menu a': 'bbFocus'
    },

    widgets: {
        library: {
            widgetClass: Pelican.ContentCarousel,
            selector: '#menu',
            options: {}
        }
    },

    onInit: function (options) {
        var self = this;
        var folders = new Pelican.Collections.ContentCollection();
        var slug = self.path.replace('home/', App.upserver.baseSlug);
        console.log('Slug: ' + self.path);

        folders.fetch({
            data: {
                slug: slug,
                depth: 1
            }
        })
            .done(function () {
                console.log('Movie ROUTE SEGMENT: ' + slug);
                var content = folders.toJSON();

                /**
                 * This is down and dirty. Should be done more neat.
                 * Depending on portal, path index could be different.
                 */
                var screen = self.path.split('/')[2];
                console.log('SCREEN: ' + screen);
                var i18t = '';
                if(screen == 'free-movies'){
                    i18t = 'Free Movies';
                    self.$('.page-title').html(i18n.t(i18t));
                } else if(screen == 'free-movies-spanish'){
                    if(App.data.language == 'en') {
                        i18t = 'Free Movies - Spanish';
                    } else if(App.data.language == 'es') {
                        i18t = 'Free Movie - Spanish';
                    }
                    self.$('.page-title').html(i18n.t(i18t));
                }

                self.getWidget('library').model.set('folders', content);
                self.getWidget('library').selectFirstFolder();
            });
    },

    onAttach: function () {

    },

    selectFolder: function (e) {
        var $obj = $(e.target);
        var slug = $obj.attr('data-slug');

        var self = this;
        var content = [];

        var assets = new Pelican.Collections.ContentCollection();
        assets.fetch({
            data: {
                slug: slug,
                depth: 1
            }
        })
            .done(function () {
                content = assets.toJSON();
            })
            .always(function () {
                self.getWidget('library').model.set('assets', content);
            });
    },

    bbFocus: function(evt){
        console.log('FOCUSING FOCUSING FOCUSING!!!!');
        var arrows = this.$('.sub-footer');
        var ct = this.$(evt.currentTarget);
        if(ct.hasClass('back-button')){
            arrows.hide();
        } else {
            arrows.show();
        }
    }
    // set "assets" model attribute, this will trigger ContentMenu to update asset list
    // this.getWidget('library').model.set('assets', children);
});

export default MovieScreen;