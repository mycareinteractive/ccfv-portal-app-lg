import Pelican from 'pelican';

require('../css/tvguide.css');
var template = require('../templates/tvguidescreen.hbs');

const TVGuideScreen = Pelican.Screen.extend({

    className: 'tvguide',
    template: template,

    channelKeyed: null,

    keyEvents: {
        'UP': 'switchChannel',
        'DOWN': 'switchChannel',
        'LEFT': 'switchProgram',
        'RIGHT': 'switchProgram',
        'ENTER': 'watchTV',
        '0': 'keyChannel',
        '1': 'keyChannel',
        '2': 'keyChannel',
        '3': 'keyChannel',
        '4': 'keyChannel',
        '5': 'keyChannel',
        '6': 'keyChannel',
        '7': 'keyChannel',
        '8': 'keyChannel',
        '9': 'keyChannel',
    },
    events: {},
    widgets: {},

    channels: [],
    channelIndex: -1,

    tzOffset: 0,

    onInit: function (options) {
        var d = new Date();
        console.log('date now : ' + d);
        console.log('offset: ' + d.getTimezoneOffset());

        this.tzOffset = (d.getTimezoneOffset() - App.data.gmtOffsetInMinute) * 60 * 1000;
        if (App.data.isDaylightSaving) {
            this.tzOffset += 3600 * 1000;
        }

        console.log('timezone offset = ' + this.tzOffset + 'ms');
    },

    onAttach: function () {
        this._buildInfo();
    },

    onScreenShow: function () {
        var self = this;
        this._buildInfo();
        if (!App.data.tvChannels) {
            var url = "/lineups/" + App.config.lineups.tv + "/channels";
            console.log("Channel Lineup Request: " + url);

            App.upserver.api(url, 'GET')
                .done(function (data) {
                    self.channels = data.sort(function (obj1, obj2) {
                        return obj1.number - obj2.number;
                    });

                    App.data.tvChannels = self.channels;
                    console.log('Channels Fetched');
                    //self._buildEpg();
                    $.doTimeout('Delay EPG', 50, self._buildEpg.bind(self));
                });
        }
        else {
            console.log('Using cached channels');
            self.channels = App.data.tvChannels;
            //self._buildEpg();
            $.doTimeout('Delay EPG', 50, self._buildEpg.bind(self));
        }
    },

    onBeforeScreenHide: function () {
        // stop clock
        $.doTimeout('tvguide clock');
    },

    switchChannel: function (e, key, $chLi) {
        var $candidates = $(), $nextProgram = $();
        var $focusedProgram = this.$('div.program.active');
        if (key == 'UP') {
            $candidates = $focusedProgram.closest('li').prev().children('div.channel').children('div.program');
            if ($candidates.length <= 0) {
                $candidates = this.$('#program li:last').children('div.channel').children('div.program');
            }
        } else if (key == 'DOWN') {
            $candidates = $focusedProgram.closest('li').next().children('div.channel').children('div.program');
            if ($candidates.length <= 0) {
                $candidates = this.$('#program li:first').children('div.channel').children('div.program');
            }
        }

        // if we pass a specific channel <li> in for direct tuning
        if ($chLi && $chLi.length > 0) {
            $candidates = $chLi.children('div.channel').children('div.program');
            if ($candidates.length <= 0) {
                $candidates = this.$('#program li:first').children('div.channel').children('div.program');
            }
        }

        // if the current program contains "now", move UP/DOWN should focus on closest program to "now"
        var begin = new Date($focusedProgram.attr('data-begin') * 1000 + this.tzOffset);
        var end = new Date($focusedProgram.attr('data-end') * 1000 + this.tzOffset);
        var now = new Date();
        var compare = begin;
        if (!begin.getTime() || (now.getTime() >= begin.getTime() && now.getTime() < end.getTime()))
            compare = now;
        $nextProgram = this._cloestProgram($candidates, compare);
        // focus on the new program and update info
        this.blur($focusedProgram);
        this._focusProgram($nextProgram);
        return true;
    },

    switchProgram: function (e, key) {
        var $candidates = $(), $nextProgram = $();
        var $focusedProgram = this.$('div.program.active');
        if (key == 'RIGHT') {
            $nextProgram = $focusedProgram.next();
            if ($nextProgram.length <= 0) {
                this.blur($focusedProgram);
                this._stepEpg('next');
                console.log(this.channelIndex);
                this._focusProgram();
                return true;
            }
        }
        if (key == 'LEFT') {
            $nextProgram = $focusedProgram.prev();
            if ($nextProgram.length <= 0) {
                this.blur($focusedProgram);
                this._stepEpg('prev');
                console.log(this.channelIndex);
                this._focusProgram();
                return true;
            }
        }
        this.blur($focusedProgram);
        this._updateInfo($nextProgram);
        this.focus($nextProgram);
        return true;
    },

    watchTV: function () {
        var ch = '2';
        var idx = this.channelIndex < 0 ? 0 : this.channelIndex;
        if (this.channels && this.channels[idx]) {
            ch = this.channels[idx].number;
        }
        App.router.go('tv?channel=' + ch);
    },

    showLabel: function (text, ms) {
        ms = ms || 5000;
        this.$('#channel-label').show().find('#loadinginfo').text(text);
        $.doTimeout('tv label');
        $.doTimeout('tv label', ms, function () {
            this.$('#channel-label').hide();
            return false;
        });
    },

    keyChannel: function (e, key) {
        var self = this;
        var cLength = 1;
        $.doTimeout('keycheck');

        var pKey = self.channelKeyed;

        if (pKey) {
            self.channelKeyed = '';
            pKey = pKey + key;

            cLength = pKey.length;
            if (cLength == 3) {
                self.keyChannelDone(pKey);
                return pKey;
            }
            key = pKey;
        }

        if (cLength == 1 && key == '0')
            return;

        this.showLabel(key, 3000);

        self.channelKeyed = key;
        $.doTimeout('keycheck', 3000, function () {
            self.keyChannelDone(self.channelKeyed);
            return false;
        }.bind(this));
        return key;
    },

    keyChannelDone: function (key) {
        console.log('in keyChannelDone ' + key);
        $.doTimeout('keycheck');
        this.channelKeyed = '';
        this.playTvByNumber(key);
        this.showLabel(key, 500);
        return;
    },

    playTvByNumber: function (num) {
        console.log('Attempting to play ' + num);
        for (var i = 0; i < this.channels.length; i++) {
            if (this.channels[i].number == num) {
                this.currentChannel = this.channels[i];
                this.channelIndex = i;
                var $li = this.$('#ch-' + this.channels[i].id).parent().parent();
                this.switchChannel(null, '', $li);
            }
        }
    },

    _buildEpg: function () {
        this._resetTimes();
        this._cleanAll();
        this._buildPrograms();
    },

    _stepEpg: function (direction) {
        if (direction == 'next')
            this._stepTimes(2);
        else if (direction == 'prev')
            this._stepTimes(-2);
        else
            return;

        this._cleanPrograms();
        this._buildPrograms();

    },

    _buildPrograms: function () {
        var context = this;
        var filename = this.dateBeg.format('yyyymmddHHMM') + '.html';

        this._getProgramFile(filename).done(function (epgfile) {
            context.$('#gridpanel-wrapper').html(epgfile);

            // remove channels not belong to this lineup
            var lineupClass = '.lineup-' + App.config.lineups.tv;
            context.$('#gridpanel #content #program li').not(lineupClass).remove();

            // get last channel if any
            if (App.data.lastChannel) {
                context.channels.find(function (ch, idx) {
                    if (ch.number == App.data.lastChannel) {
                        context.channelIndex = idx;
                        return true;
                    }
                    return false;
                });
            }

            context._focusProgram();
            context.$('#mask').hide();

            console.log('build programs complete');
        });

    },

    _buildInfo: function () {
        // start clock
        var context = this;
        $.doTimeout('tvguide clock'); // kill first
        $.doTimeout('tvguide clock', 10000, function () {
            var d = new Date();
            var format = 'dddd, mmm d | h:MM TT';
            var locale = App.data.language || 'en';
            context.$('#gridinfo #current p.date').text(d.format(format, false, locale));
            return true;
        });
        $.doTimeout('tvguide clock', true); // do it now
    },

    _updateInfo: function ($obj) {
        console.log($obj);
        if (!$obj) {
            this.$('#gridinfo #current p.channel').empty();
            this.$('#gridinfo #current p.title').empty();
            this.$('#gridinfo #current p.time').empty();
            return;
        }

        // update channel info
        var $ch = $obj.parent().prev();
        this._updateChannelInfo($ch);

        // update program info
        var name = $obj.attr('title');
        var episode = $obj.attr('data-epi-num') + ' ' + $obj.attr('data-epi-title');
        var description = $obj.attr('data-description');
        var id = $obj.attr('id');
        var beg = new Date($obj.attr('data-begin') * 1000 + this.tzOffset);
        var end = new Date($obj.attr('data-end') * 1000 + this.tzOffset);
        var duration = $obj.attr('data-duration');

        if (id == 'na') {
            this.$('#gridinfo #current p.title').text(name);
            this.$('#gridinfo #current p.time').empty();
        }
        else {
            this.$('#gridinfo #current p.title').text(name);
            this.$('#gridinfo #current p.episode').text(episode);
            this.$('#gridinfo #current p.description').text(description);
            this.$('#gridinfo #current p.time').text(beg.format('h:MMtt') + ' - ' + end.format('h:MMtt'));
        }
    },

    _updateChannelInfo: function ($obj) {
        var context = this;
        if (!$obj || $obj.length < 1)
            return;

        this.$('#gridinfo #current p.title').empty();
        this.$('#gridinfo #current p.time').empty();

        var chId = $obj.find('.channelname').attr('id');
        this.channels.find(function (ch, idx) {
            if ('ch-' + ch.id == chId) {
                context.channelIndex = idx;
                return true;
            }
            return false;
        });

        var chName = $obj.find('.channelname').find('span').text();
    },

    _focusProgram: function ($program) {
        var context = this;
        this.channelIndex = this.channelIndex < 0 ? 0 : this.channelIndex;
        if (this.channelIndex < 0 || this.channelIndex >= this.channels.length)
            return;

        var $chLi;

        if (!$program || $program.length <= 0) {
            // find current channel and program
            var ch = this.channels[this.channelIndex];
            var $ch = context.$('div#programs-' + ch.id);
            $program = this._cloestProgram($ch.find('.program'), this.dateNow);
            $chLi = $ch.parent();
        }
        else {
            $chLi = $program.parent().parent();
        }

        this._updateInfo($program);
        this.focus($program);

        // scroll if needed
        this._scrollToChannel($chLi);
    },

    _scrollToChannel: function ($chLi) {
        var context = this;
        if (!$chLi || $chLi.length <= 0) {
            // find current channel
            if (this.channelIndex < 0 || this.channelIndex >= this.channels.length)
                return;
            var ch = this.channels[this.channelIndex];
            $chLi = this.$('div#' + ch.channelID).parent();
        }
        var $container = this.$('#gridpanel #content');
        var scrollTop = $container.scrollTop();
        var baseTop = $container.offset().top;
        var baseBottom = $container.height() + baseTop - 5;
        var chTop = $chLi.offset().top;

        console.log('scrolling to ' + this.channelIndex + 'th channel');

        if ($chLi.is(':first-child')) { // first channel, always scroll to top
            $container.scrollTop(0);
        }
        else if ($chLi.is(':last-child')) {
            $container.scrollTop(Math.floor($container[0].scrollHeight));
        }

        var iter = 0;  // loop safety check

        if(chTop < baseTop) {
            while (chTop < baseTop && iter < 30) { // need to scroll up
                iter++;
                $container.scrollTop(Math.floor(scrollTop - $chLi.height() * 7));
                chTop = $chLi.offset().top;
                scrollTop = $container.scrollTop();
            }
        }
        else {
            while (chTop >= baseBottom && iter < 30) { // need to scroll down
                iter++;
                $container.scrollTop(Math.floor(scrollTop + $chLi.height() * 7));
                chTop = $chLi.offset().top;
                scrollTop = $container.scrollTop();
            }
        }
        console.log('scrolling complete');
    },

    _getProgramFile: function (filename) {
        var self = this;
        var defer = $.Deferred();
        var epgname = this.cachedEpgName;
        var epgfile = this.cachedEpgFile;
        if (!epgname || !epgfile || epgname != filename) {
            var url = App.config.epgFilePath + filename;
            $.get(url).done(function (epgfile) {
                console.log('Epg file ' + filename + ' loaded');
                if (epgfile) {
                    self.cachedEpgName = filename;
                    self.cachedEpgFile = epgfile;
                }
                defer.resolve(epgfile);
            }).fail(function () {
                console.log('Failed to get epg file ' + url);
                defer.reject();
            });
        }
        else {
            console.log('Using cached epg file ' + filename);
            defer.resolve(epgfile);
        }
        return defer.promise();
    },

    _cleanAll: function () {
        this.$('#gridinfo #current p.channel').empty();
        this.$('#gridinfo #current p.title').empty();
        this.$('#gridinfo #current p.time').empty();
        this.$("#gridpanel-wrapper").empty();
    },

    _cleanPrograms: function () {
        this.$("#gridpanel #program .channel").remove();
        this.$('#gridinfo #current p.title').empty();
        this.$('#gridinfo #current p.time').empty();
    },

    // set EPG window to current
    _resetTimes: function () {
        this.dateNow = new Date();
        var d = new Date(this.dateNow);
        d.setMilliseconds(0);
        d.setSeconds(0);
        d.setMinutes(0);
        this.dateBeg = new Date(d);
        this.dateEnd = new Date(d);

        var h = this.dateNow.getHours();
        this.dateBeg.setHours(Math.floor(h / 2) * 2);
        this.dateEnd.setHours(Math.floor(h / 2) * 2 + 2);
    },

    // step begin and end time of the EPG window
    _stepTimes: function (hours) {
        this.dateBeg = this.dateBeg.addHours(hours);
        this.dateEnd = this.dateEnd.addHours(hours);
    },

    // parse EPG time format '2013-05-30 13:14:00.0'
    _parseEpgTime: function (str) {
        var dateTime = str.split(" ");

        var date = dateTime[0].split("-");
        var yyyy = date[0];
        var mm = date[1] - 1;
        var dd = date[2];

        var time = dateTime[1].split(":");
        var h = time[0];
        var m = time[1];
        var s = parseInt(time[2]); //get rid of that 00.0;

        return new Date(yyyy, mm, dd, h, m, s);
    },

    // adjust time to nearest 30min slot
    _roundUpTime: function (datetime) {
        var d = new Date(datetime);
        var m = (Math.round(d.getMinutes() / 30) * 30) % 60;
        var h = (d.getMinutes() >= 45) ? d.getHours() + 1 : d.getHours();
        d.setMilliseconds(0);
        d.setSeconds(0);
        d.setMinutes(m);
        d.setHours(h);
        return d;
    },

    // calculate begin, time and duration for specific program
    _calcDuration: function (startTime, endTime) {
        var duration = 0;
        var beg = this._parseEpgTime(startTime);
        var end = this._parseEpgTime(endTime);

        beg = this._roundUpTime(beg);
        end = this._roundUpTime(end);

        // adjust program if it begins earlier or end later than current epg window
        if (beg.getTime() < this.dateBeg.getTime())
            beg = this.dateBeg;
        if (end.getTime() > this.dateEnd.getTime())
            end = this.dateEnd;

        var diff = (end.getTime() - beg.getTime()) / 1000 / 60;
        diff = Math.floor(diff / 30) * 30;
        duration = diff > 0 ? diff : 0;

        var ret = Array();
        ret['duration'] = duration;
        ret['begdate'] = beg;
        ret['enddate'] = end;
        return ret;
    },

    // Pick a program from candidates that is currently showing based on the provided time.
    // If no one is showing, pick one that is closest to the start time
    _cloestProgram: function ($candidates, time) {
        var self = this;
        if ($candidates.length < 1)
            return $();
        if ($candidates.length == 1)
            return $candidates;

        if (!time.getTime()) {
            // begin time not valid, just pick the first candidate
            return $candidates.first();
        }

        var $ret = $();
        var min = -1;
        $candidates.each(function () {
            var begin = new Date($(this).attr('data-begin') * 1000 + self.tzOffset);
            var end = new Date($(this).attr('data-end') * 1000 + self.tzOffset);

            // on air?
            if (time.getTime() >= begin.getTime() && time.getTime() < end.getTime()) {
                $ret = $(this);
                return false;
            }

            // calculate difference
            var diff = begin.getTime() - time.getTime();
            diff = (diff < 0) ? -diff : diff;
            if (min == -1 || diff < min) {
                min = diff;
                $ret = $(this);
            }
        });
        return $ret;
    }
});

export default TVGuideScreen;