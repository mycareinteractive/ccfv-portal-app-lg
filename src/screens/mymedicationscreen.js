import SecondaryScreen from './secondaryscreen';
import i18n from 'i18next';
import Pelican from 'pelican';

require('../css/mymedicationscreen.css');
var template = require('../templates/mymedicationsscreen.hbs');

const MyMedicationScreen = SecondaryScreen.extend({

    className: 'mymedications',

    template: template,

    widgets: {
        dialog: {
            widgetClass: Pelican.Dialog,
            selector: '#dialog',
            options: {hidden: true}
        },

        menu: {
            widgetClass: Pelican.TabMenu,
            selector: '#menu'
        }
    },

    keyEvents: function () {
        return {};
    },

    events: function () {
        return $.extend({}, SecondaryScreen.prototype.events, {
            'click #accept': 'selectAccept',
            'click #decline': 'selectDecline',
            'click .requestdishcargemeds': 'sendInboxRequest',
            'click .requestpharvisit': 'sendInboxRequest'
        })
    },

    onInit: function (options) {
        var slug = this.path.replace('home/', App.upserver.baseSlug);
        console.log('MedicationScreen Slug: ' + this.path);

        var self = this;

        // get self content
        var parent = new Pelican.Models.Content();
        parent.fetch({
            data: {
                slug: slug
            }
        }).done(function () {
            var pcont = parent.toJSON();
            self.$('.page-title').first().html(pcont.content.metadatas.title);
            self.getWidget('menu').model.set({parent: pcont.content});
        });

        var contents = new Pelican.Collections.ContentCollection();
        contents.fetch({
            data: {
                slug: slug,
                depth: 2
            }
        }).done(function () {
            console.log('APP ROUTE SEGMENT: ' + slug);
            var content = contents.toJSON();
            var model = {menu: content};
            var lang = (App.data.language || 'english').toLowerCase();

            self.getRxInfo('sch_' + lang)
                .done(function (schcontent) {
                    if (schcontent != '')
                        self.$('#menu1-tab .sub-content').html(schcontent);
                    else
                        self.$('#menu1-tab .sub-footer').html('');

                    self.getWidget('menu').click('#menu1');
                });


            self.getRxInfo('prn_' + lang)
                .done(function (prncontent) {
                    if (prncontent != '')
                        self.$('#menu2-tab .sub-content').html(prncontent);
                    else
                        self.$('#menu2-tab .sub-footer').html('');
                });
            self.getWidget('menu').model.set(model);
            self.getWidget('menu').click('#menu1');

            self.showAcceptance();

        });
    },

    showAcceptance: function () {
        var self = this;

        var p1 = i18n.t('Please read the terms and conditions to the right, then indicate your acceptance or declination of these terms and conditions by pressing Select on Accept or Decline below.') + '<br>';
        var p2 = i18n.t('Welcome to the UpCare™ system at Cleveland Clinic Avon Hospital. This digital system engages patients and their families by displaying meaningful information within the patient\'s room.');
        var p3 = i18n.t('Some information, such as your name and/or medical condition, may be displayed on this TV screen when using this product. Some of this information is considered protected health information, as defined under the Health Insurance Portability and Accountability Act that was passed by Congress in 1996.');
        var p4 = i18n.t('By using the UpCare™ system you are hereby consenting to such information being displayed, which may be seen by third parties who are present in the room at the time the information is displayed. ');

        var html =
            '<div class="english regtext">' +
            '<p>' + p1 + '</p><br>' +
            '</div><div class="regtext2">' +
            '<p>' + p2 + '</p><br>' +
            '<p>' + p3 + '</p><br>' +
            '<p>' + p4 + '</p><br>' +
            '</div>';


        var model = {
            buttons: [
                {id: 'accept', text: i18n.t('Accept'), className: "bottom"},
                {id: 'decline', text: i18n.t('Decline') + ' / ' + i18n.t('Cancel'), className: "bottom"}
            ],
            content: html
        };
        self.getWidget('dialog').model.set(model);
        self.getWidget('dialog').show();

        self.focus('#accept');
    },

    selectAccept: function () {
        var self = this;
        self.getWidget('dialog').hide();
    },

    selectDecline: function () {
        this.back()
    },

    getRxInfo: function (type) {
        var self = this;
        var defer = jQuery.Deferred();

        var htmlstring1 = '<div class="sub-page" style="display: block;"><div class="sub-text1"><br/><br/>';
        var htmlstring2 = '</div><div class="sub-image1"><img src="images/popup_careboard.png"></div></div' + '>';
        var rxtext = '';
        upserver.api('/me/clinicalrecords?type=' + type + '&sort=value', 'GET')
            .done(function (data) {
                if (!data || data.length < 1) {
                    defer.resolve('');
                }
                for (var i = 0; i < data.length; i++) {
                    var rx = data[i].value;
                    rxtext = rxtext + htmlstring1 + rx + htmlstring2;
                    console.log(rx);
                }
                console.log(rxtext);
                defer.resolve(rxtext);
            })
            .fail(function (f) {
                defer.reject(f.errorMessage);
            });

        return defer.promise();
    },

    showPharNotification: function () {
        var self = this;
        self.$('#pharvisit-notification').show();

        $.doTimeout(5000, function () {
            self.$('#pharvisit-notification').hide();
        });
    },

    showDisMedsNotification: function () {
        var self = this;
        self.$('#dischargemeds-notification').show();

        $.doTimeout(5000, function () {
            self.$('#dischargemeds-notification').hide();
        });

    },

    sendInboxRequest: function (e) {
        var mrn = App.data.patient.mrn;
        var fullname = App.data.patient.patientProfile.fullName;
        var acct = App.data.patient.patientVisit.accountNumber;
        var dob = App.data.patient.patientProfile.dateOfBirth;
        var type = $(e.target).attr('id');
        var url = App.config.mirth.host + "/?mrn=" + mrn + "&fullname=" + fullname + "&acct=" + acct + "&dob=" + dob + "&type=" + type;
        App.upserver.api('/proxy', 'GET', {
            "url": url
        });
        console.log('message sent' + url);

        App.tracker.event('medication', type);

        if (type == 'pharvisit')
            this.showPharNotification();
        else
            this.showDisMedsNotification();
    }

});

export default MyMedicationScreen;