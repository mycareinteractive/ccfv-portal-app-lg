import Pelican from 'pelican';
var packageJson = require("../../package.json");

require('../css/discharged.css');

var template = require('../templates/dischargedscreen.hbs');

const DischargedScreen = Pelican.Screen.extend({

    className: 'discharged',

    template: template,

    keyEvents: {
        'POWER': true,
        'EXIT': true,
        'BACK': true,
        'MENU': true,
        'HOME': true,
        'CLOSE': true,
        'CHUP': 'watchTv',
        'CHDN': 'watchTv'
    },

    events: {},

    widgets: {
        datetime: {
            widgetClass: Pelican.DigitalClock,
            selector: '#time',
            options: {format: 'h:MM tt', locale: 'en'}
        },
        datetime2: {
            widgetClass: Pelican.DigitalClock,
            selector: '#date',
            options: {format: 'dddd, mmm. d', locale: 'en'}
        },
    },

    onAttach: function () {
        var self = this;

        if (App.data && App.data.device && App.data.device.deviceProvision && App.data.device.deviceProvision.location) {
            var location = App.data.device.deviceProvision.location;
            var room = location.room;
            if (location.bed != '00') {
                room = room + ' ' + location.bed;
            }
            this.$('#room').text('Room ' + room);
        }

        var pathnames = window.location.pathname.split('/');
        var appName = pathnames[pathnames.length - 2] || '';
        this.$('#app-version').text(appName + ' ' + packageJson.version);
    },

    watchTv: function () {
        App.router.go('discharged/tv');
    }

});

export default DischargedScreen;