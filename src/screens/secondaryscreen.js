import Pelican from 'pelican';

require('../css/secondary.css');
var template = require('../templates/secondaryscreen.hbs');

const SecondaryScreen = Pelican.Screen.extend({

    className: 'secondary',

    template: template,

    regions: {
        'title': {
            selector: '.page-title'
        }
    },

    keyEvents: {},

    events: {
        'click .back-button': 'back',
        'focus .menu-tab-button': 'menuFocused'
    },

    widgets: {
        menu: {
            widgetClass: Pelican.TabMenu,
            selector: '#menu'
        }
    },

    onInit: function (options) {
        var slug = this.path.replace('home/', App.upserver.baseSlug);
        console.log('Slug: ' + slug);

        var self = this;

        // get self content
        var parent = new Pelican.Models.Content();
        parent.fetch({
            data: {
                slug: slug
            }
        }).done(function () {
            var pcont = parent.toJSON();
            self.$('.page-title').first().html(pcont.content.metadatas.title);
            self.getWidget('menu').model.set({parent: pcont.content});
        });


        var contents = new Pelican.Collections.ContentCollection();
        contents.fetch({
            data: {
                slug: slug,
                depth: 2
            }
        }).done(function () {
            console.log('APP ROUTE SEGMENT: ' + slug);
            var scont = contents.toJSON();
            var model = {menu: scont};
            self.getWidget('menu').model.set(model);
            self.click('#menu1');
        });

    },

    onAttach: function () {

    },

    menuFocused: function (e) {
        var $item = $(e.target);
        var path = $item.attr('data-slug').replace(App.upserver.baseSlug, 'home/');
        var id = $item.attr('data-current-slug').replace(/[\.:#]/g, '-');;
        if(path) {
            App.tracker.pageView(path, id);
        }
    }
});

export default SecondaryScreen;