// agent-shim is needed to make sure Marionette Inspector works in Chrome
import './helpers/agent-shim';
import upserver from 'upserver-client';
import Pelican from 'pelican';
import i18next from 'i18next';
import XHR from 'i18next-xhr-backend';
import Router from './router';
import Scheduler from './services/scheduler';
import PushHandler from './services/pushhandler';
import EHospital from './services/ehospital';

// Detect platform
if (typeof window['EONimbus'] != 'undefined') {
    console.log("I think we are Enseo");
}
else if (typeof window['EONebula'] != 'undefined') {
    console.log("I think we are Nebula");
}
else if (navigator.userAgent.indexOf(';LGE ;') > -1) {
    var Procentric = require('pelican-procentric');
    window.PelicanDevice = new Procentric();
    console.log("I think we are LG Procentric");
}
else {
    window.PelicanDevice = new Pelican.Desktop();
    console.log("I don't know, guess we are PC");
}

require('./css/index.css');
require('./css/global.css');

window.i18next = window.i18n = i18next;

// Instantiate App and main players
const App = window.App = new Pelican.Application({
    router: new Router(),
    layout: new Pelican.StackLayout(),
    tracker: new Pelican.Tracker(),
    scheduler: new Scheduler(),
    pushHandler: new PushHandler(),
    eHospital: new EHospital()
});

App.upserver = upserver;
App.device = window.PelicanDevice;


// render the app
App.screens = App.layout.screens; // a shorthand to access all screens
App.layout.render('#app');

// Override this function when you need to do something synchronously before app start
App.onBeforeStart = function () {
    console.log('App before start');
};

// Override this function when you need to do something asynchronously before app start
// Call the this.start() when you are all set.
App.onConfigReady = function () {
    var self = this;
    console.log('Config ready');
    console.log('Beam me up, Scotty...');

    var config = App.config;

    App.upserver = upserver;
    App.upserver.server = App.config.server;
    App.upserver.baseSlug = App.config.baseSlug;
    App.upserver.baseUrl = App.server + App.config.baseSlug;
    App.upserver.appId = App.config.appId;

    var device = PelicanDevice.getNetworkDevices()[0].mac.replace(/:/g, '');
    if (PelicanDevice.getPlatform() == 'DESKTOP' && App.config.desktopDeviceId) {
        device = App.config.desktopDeviceId;
    }

    if(PelicanDevice.getPlatform() == "PROCENTRIC"){
        var sc = App.config.startChannel || {};
        PelicanDevice.setStartChannel(sc);
    }

    App.data = {};
    App.data.deviceId = device;
    App.data.ip = PelicanDevice.getNetworkDevices()[0].ip;
    App.data.firmware = PelicanDevice.getMiddlewareVersion();

    console.log('DEVICES: ');
    console.log(device);

    $.getJSON(App.config.server + '/api/v1/time')
        .done(function (data) {
            console.log('Server time: ' + JSON.stringify(data));
            // LG has odd logic for timezone.  Say for ESTEDT, they want -240 instead of 300
            App.data.gmtOffsetInMinute = data.gmtOffsetInMinute;
            App.data.isDaylightSaving = data.isDaylightSaving;
            PelicanDevice.setTime(data);
        })
        .fail(function () {
            console.log("!!! TIME REQUEST FAILED !!!");
        });

    // If app does not init in 15 minutes, reboot
    $.doTimeout('app init reset', 900000, function(){
        if (PelicanDevice.reboot)
            PelicanDevice.reboot();
    });

    // initialize upserver
    App.upserver.init({
        host: App.upserver.server,
        clientId: App.upserver.appId,
        deviceId: device,
        disableSignalR: config.disableSignalR,
        transport: config.signalRTransport
    })
        .done(function () {
            console.log('server authentication ok');

            // get patient first
            App.scheduler.getDevice().always(function () {
                App.data.language = 'en';

                App.scheduler.getPatient().always(function () {

                    App.tracker.init(
                        config.analyticsProvider,
                        config.analyticsUrl,
                        config.analyticsSite,
                        App.data.device.deviceProvision.location.roomBed
                    );

                    var patient = App.data.patient;
                    if (patient && patient.patientVisit) {
                        App.tracker.setUserId(patient.patientVisit.id);
                    }

                    if (patient && patient.patientPreferences) {
                        var langs = $.grep(patient.patientPreferences, function (e) {
                            return e.key == 'language'
                        });
                        if (langs && langs[0] && langs[0].value) {
                            App.data.language = langs[0].value;
                        }
                    }

                    // set API lang header
                    App.upserver.locale = App.data.language;
                    App.upserver.setLanguage(App.data.language);  // set client API cookie

                    // load i18n static resources
                    i18next
                        .use(XHR)
                        .init({
                            lng: 'en',
                            preload: ['en', 'es'],
                            debug: true,
                            fallbackLng: false,
                            nsSeparator: false,
                            keySeparator: false,
                            returnEmptyString: false,
                            backend: {
                                "loadPath": "locales/{{lng}}/{{ns}}.json"
                            }
                        }, function (err, t) {
                            i18next.changeLanguage(App.data.language, function() {
                                $.doTimeout('app init reset'); // cancel reset timer
                                self.start();
                                App.scheduler.start();
                                App.pushHandler.start();
                                App.eHospital.start();
                            });
                        });

                });
            });
        })
        .fail(function () {
            console.log('server authentication failed');
        });

};

window.app = App;

// Kick off everything
App.bootstrapDevice();

export default App;

