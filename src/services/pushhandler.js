class PushHandler {

    start(options) {
        upserver.on(upserver.events.device.patientAssociated, this.patientAssociated.bind(this));
        upserver.on(upserver.events.device.patientDissociated, this.patientDissociated.bind(this));
    }

    reset() {
        if (PelicanDevice.reboot) {
            console.log('About to reboot...');
            PelicanDevice.reboot();
        }
    }

    patientAssociated() {
        console.log('patient associated, will reboot!');
        this.reset();
    }

    patientDissociated() {
        console.log('patient dessociated, will reboot!');
        this.reset();
    }

}

export default PushHandler;